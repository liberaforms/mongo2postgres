"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from psycopg2 import connect, extensions

def postgres_db_connect(dbname="postgres"):
    try:
        conn = connect(
            dbname = dbname,
            user = os.getenv("POSTGRES_ROOT_USER"),
            password = os.getenv("POSTGRES_ROOT_PASSWORD"),
            host = os.getenv("POSTGRES_HOST")
        )
        return conn
    except Exception as error:
        print(str(error))
        exit()

def create_database(db_user, db_pass, db_name):
    conn = postgres_db_connect()
    autocommit = extensions.ISOLATION_LEVEL_AUTOCOMMIT
    conn.set_isolation_level( autocommit )
    cursor = conn.cursor()
    sql=f"CREATE DATABASE {db_name} ENCODING 'UTF8' TEMPLATE template0;"
    print(sql)
    try:
        cursor.execute(sql)
    except Exception as error:
        print(str(error))
        exit()
    sql=f"CREATE USER {db_user} WITH PASSWORD '{db_pass}';"
    print(sql)
    try:
        cursor.execute(sql)
    except Exception as error:
        print(str(error))
    sql=f"GRANT ALL PRIVILEGES ON DATABASE {db_name} TO {db_user};"
    print(sql)
    try:
        cursor.execute(sql)
    except Exception as error:
        print(str(error))
        exit()
    cursor.close()
    conn.close()
    print("\nDatabase created ok")
