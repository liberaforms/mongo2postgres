"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from datetime import datetime, timezone
from dotenv import load_dotenv
import sqlalchemy as db
from sqlalchemy import create_engine
from sqlalchemy.ext.mutable import MutableDict, MutableList
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql import JSONB, ARRAY, TIMESTAMP
from sqlalchemy.orm import Session, relationship

from pprint import pprint


load_dotenv('sqlalchemy.ini')
db_uri = os.environ['SQLALCHEMY_DATABASE_URI']
engine = create_engine(db_uri)
Base = declarative_base()
pg_session = Session(engine)


""" Temporary table to store ids
    To be dropped after migration
"""
class IDs(Base):
    __tablename__ = "temp_ids"
    id = db.Column(db.Integer, primary_key=True, index=True)
    model = db.Column(db.String, nullable=False)
    mongo_id = db.Column(db.String, nullable=False)
    pg_id = db.Column(db.Integer, nullable=False)

    def __init__(self, model, mongo_id, pg_id):
        self.model = model
        self.mongo_id = mongo_id
        self.pg_id = pg_id

class Site(Base):
    __tablename__ = "site"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP,
                        default=datetime.now(timezone.utc),
                        nullable=False)
    hostname = db.Column(db.String, nullable=False)
    port = db.Column(db.Integer, nullable=True)
    scheme = db.Column(db.String, nullable=False, default="http")
    siteName = db.Column(db.String, nullable=False)
    defaultLanguage = db.Column(db.String, nullable=False)
    primary_color = db.Column(db.String, nullable=False)
    invitationOnly = db.Column(db.Boolean, default=True)
    consentTexts = db.Column(ARRAY(JSONB), nullable=False)
    newUserConsentment = db.Column(JSONB, nullable=True)
    smtpConfig = db.Column(JSONB, nullable=False)
    newuser_enableuploads = db.Column(db.Boolean, nullable=False, default=False)
    mimetypes = db.Column(JSONB, nullable=False)
    email_footer = db.Column(db.String, nullable=True)
    blurb = db.Column(JSONB, nullable=False)

    def __init__(self, **kwargs):
        self.created = kwargs['created']
        self.hostname = kwargs['hostname']
        self.port = kwargs['port']
        self.scheme = kwargs['scheme']
        self.siteName = kwargs['siteName']
        self.defaultLanguage = kwargs['defaultLanguage']
        self.primary_color = kwargs['menuColor']
        self.invitationOnly = kwargs['invitationOnly']
        self.consentTexts = kwargs['consentTexts']
        self.newUserConsentment = kwargs['newUserConsentment']
        self.smtpConfig = kwargs['smtpConfig']
        self.blurb = kwargs['blurb']
        self.mimetypes = {
                "extensions": ["pdf", "png", "odt"],
                "mimetypes": ["application/pdf",
                              "image/png",
                              "application/vnd.oasis.opendocument.text"]
        }

class Invite(Base):
    __tablename__ = "invites"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP,
                        default=datetime.now(timezone.utc),
                        nullable=False)
    email = db.Column(db.String, nullable=False)
    message = db.Column(db.String, nullable=True)
    token = db.Column(JSONB, nullable=False)
    admin = db.Column(db.Boolean, default=False)

    def __init__(self, **kwargs):
        self.email = kwargs['email']
        self.message = kwargs['message']
        self.token = kwargs['token']
        self.admin = kwargs['admin']

class User(Base):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP,
                        default=datetime.now(timezone.utc),
                        nullable=False)
    username = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)
    password_hash = db.Column(db.String, nullable=False)
    preferences = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    blocked = db.Column(db.Boolean, default=False)
    admin = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    validatedEmail = db.Column(db.Boolean, default=False)
    uploads_enabled = db.Column(db.Boolean, default=False, nullable=False)
    token = db.Column(JSONB, nullable=True)
    consentTexts = db.Column(ARRAY(JSONB), nullable=True)
    authored_forms = relationship("Form", cascade = "all, delete, delete-orphan")
    timezone = db.Column(db.String, nullable=True)
    media = relationship("Media",
                            lazy='dynamic',
                            cascade = "all, delete, delete-orphan")

    def __init__(self, **kwargs):
        self.created = kwargs['created']
        self.username = kwargs['username']
        self.email = kwargs['email']
        self.password_hash = kwargs['password_hash']
        self.preferences = kwargs['preferences']
        self.blocked = kwargs['blocked']
        self.admin = kwargs['admin']
        self.validatedEmail = kwargs['validatedEmail']
        self.token = kwargs['token']
        self.consentTexts = kwargs['consentTexts']

class Form(Base):
    __tablename__ = "forms"
    _site=None
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP,
                        default=datetime.now(timezone.utc),
                        nullable=False)
    slug = db.Column(db.String, unique=True, nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    structure = db.Column(MutableList.as_mutable(ARRAY(JSONB)), nullable=False)
    fieldIndex = db.Column(MutableList.as_mutable(ARRAY(JSONB)), nullable=False)
    editors = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    enabled = db.Column(db.Boolean, default=False)
    expired = db.Column(db.Boolean, default=False)
    sendConfirmation = db.Column(db.Boolean, default=False)
    expiryConditions = db.Column(JSONB, nullable=False)
    sharedAnswers = db.Column(MutableDict.as_mutable(JSONB), nullable=True)
    shared_notifications = db.Column(MutableList.as_mutable(ARRAY(db.String)), nullable=False)
    restrictedAccess = db.Column(db.Boolean, default=False)
    adminPreferences = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    introductionText = db.Column(JSONB, nullable=False)
    afterSubmitText = db.Column(JSONB, nullable=False)
    expiredText = db.Column(JSONB, nullable=False)
    consentTexts = db.Column(ARRAY(JSONB), nullable=True)
    author = relationship("User", back_populates="authored_forms")
    answers = relationship("Answer", lazy='dynamic',
                                        cascade="all, delete, delete-orphan")
    log = relationship("FormLog", lazy='dynamic',
                                     cascade="all, delete, delete-orphan")

    def __init__(self, **kwargs):
        self.created = kwargs['created']
        self.author_id = kwargs['author_id']
        self.slug = kwargs['slug']
        self.editors = kwargs['editors']
        self.enabled = kwargs['enabled']
        self.expired = kwargs['expired']
        self.sendConfirmation = kwargs['sendConfirmation']
        self.expiryConditions = kwargs['expiryConditions']
        self.structure = kwargs['structure']
        self.fieldIndex = kwargs['fieldIndex']
        self.sharedAnswers = kwargs['sharedAnswers']
        self.shared_notifications = []
        self.restrictedAccess = kwargs['restrictedAccess']
        self.adminPreferences = kwargs['adminPreferences']
        self.introductionText = kwargs['introductionText']
        self.afterSubmitText = kwargs['afterSubmitText']
        self.expiredText = kwargs['expiredText']
        self.consentTexts = kwargs['consentTexts']

class Answer(Base):
    __tablename__ = "answers"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP,
                        default=datetime.now(timezone.utc),
                        nullable=False)
    form_id = db.Column(db.Integer, db.ForeignKey('forms.id'), nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    marked = db.Column(db.Boolean, default=False)
    data = db.Column(JSONB, nullable=False)
    form = relationship("Form", viewonly=True)
    attachments = relationship(  "AnswerAttachment",
                                    lazy='dynamic',
                                    cascade = "all, delete, delete-orphan")

    def __init__(self, **kwargs):
        self.created = kwargs['created']
        self.author_id = kwargs['author_id']
        self.form_id = kwargs['form_id']
        self.marked = kwargs['marked']
        self.data = kwargs['data']

class AnswerAttachment(Base):
    __tablename__ = "attachments"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP,
                        default=datetime.now(timezone.utc),
                        nullable=False)
    answer_id = db.Column(db.Integer, db.ForeignKey('answers.id',
                                                    ondelete="CASCADE"),
                                                    nullable=True)
    form_id = db.Column(db.Integer, db.ForeignKey('forms.id'), nullable=False)
    file_name = db.Column(db.String, nullable=False)
    storage_name = db.Column(db.String, nullable=False)
    local_filesystem = db.Column(db.Boolean, default=True) #Remote storage = False
    file_size = db.Column(db.String, nullable=False)
    encrypted = db.Column(db.Boolean, default=False)
    form = relationship("Form", viewonly=True)


class FormLog(Base):
    __tablename__ = "form_logs"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP,
                        default=datetime.now(timezone.utc),
                        nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    form_id = db.Column(db.Integer, db.ForeignKey('forms.id',
                                                    ondelete="CASCADE"),
                                                    nullable=True)
    message = db.Column(db.String, nullable=False)
    user = relationship("User", viewonly=True)
    form = relationship("Form", viewonly=True)

    def __init__(self, **kwargs):
        self.created = kwargs['created']
        self.user_id = kwargs['user_id']
        self.form_id = kwargs['form_id']
        self.message = kwargs['message']

class Media(Base):
    __tablename__ = "media"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP,
                        default=datetime.now(timezone.utc),
                        nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id',
                                                  ondelete="CASCADE"),
                                                  nullable=False)
    alt_text = db.Column(db.String, nullable=True)
    file_name = db.Column(db.String, nullable=False)
    file_size = db.Column(db.String, nullable=False)
    storage_name = db.Column(db.String, nullable=False)
    local_filesystem = db.Column(db.Boolean, default=True) #Remote storage = False
    user = relationship("User", viewonly=True)


Base.metadata.create_all(engine, tables=[IDs.__table__])
