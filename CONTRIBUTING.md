
## Commit messages

Messages should be useful.

An example message.
```
database schema: change port property type from string to integer

A port number is a 16-bit unsigned integer.
Using Integer makes the database schema more explicit.

Port numbers are described at
https://en.wikipedia.org/wiki/Port_(computer_networking)

Fixes #21
```

### First line
The first line is a short one-line summary of the change, prefixed by ~ two word `context` and `:`

No capital letters and no trailing `.`

### Main content
Provides more context and explains what it does. Write in complete sentences.
Don't use Markdown or any other markup language.

If the commit closes an issue, append `Fixes #<issue_id>`
