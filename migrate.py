"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from dotenv import load_dotenv
from mongo import mongo
from postgres import models
from postgres.models import pg_session, engine

from pprint import pprint

if not os.path.exists('sqlalchemy.ini'):
    print("Please run 'python create_db.py' first")
    exit()

load_dotenv(".env")
load_dotenv('sqlalchemy.ini')

HOSTNAME=os.getenv("SITE_HOST_NAME")
try:
    mongo_db = mongo.mongo_db_connect()
except Exception as error:
    print(str(error))
    exit()


""" Import 'Site' """
print("Importing site table")
mongo_site = mongo.get_site(mongo_db, HOSTNAME)
port = mongo_site['port'] if 'port' in mongo_site else None
scheme = mongo_site['scheme'] if 'scheme' in mongo_site else "http"
newuser_consent=mongo_site['newUserConsentment'] if 'newUserConsentment' in mongo_site else []
pg_site = models.Site(
        created=mongo_site['created'],
        hostname=HOSTNAME,
        port=port,
        siteName=mongo_site['siteName'],
        defaultLanguage=mongo_site['defaultLanguage'],
        menuColor=mongo_site['menuColor'],
        scheme=scheme,
        blurb=mongo_site['blurb'],
        invitationOnly=mongo_site['invitationOnly'],
        consentTexts=mongo_site['consentTexts'],
        newUserConsentment=newuser_consent,
        smtpConfig=mongo_site['smtpConfig']
    )
pg_session.add(pg_site)
pg_session.commit()
print("Ok")

""" Import 'Invites' """
print("Importing invite table")
mongo_invites = mongo.get_invites(mongo_db, HOSTNAME)
for mongo_invite in mongo_invites:
    mongo_invite['token']['created'] = str(mongo_invite['token']['created'])
    pg_invite = models.Invite(
        email=mongo_invite['email'],
        message=mongo_invite['message'],
        token=mongo_invite['token'],
        admin=mongo_invite['admin'],
    )
    pg_session.add(pg_invite)
pg_session.commit()
print("Ok")

""" Import 'Users' 'Forms' 'Entries' """
print("\n")
mongo_users = mongo.get_users(mongo_db, HOSTNAME)
for mongo_user in mongo_users:
    print(f"Importing User: {mongo_user['username']}")
    if 'token' in mongo_user:
        if 'created' in mongo_user['token']:
            mongo_user['token']['created'] = str(mongo_user['token']['created'])
        token = mongo_user['token']
    else:
        token = {}
    preferences = {
        "language": mongo_user['preferences']['language'],
        "newAnswerNotification": mongo_user['preferences']['newEntryNotification']
    }
    pg_user = models.User(
        created=mongo_user['created'],
        username=mongo_user['username'],
        email=mongo_user['email'],
        password_hash=mongo_user['password'],
        preferences=preferences,
        blocked=mongo_user['blocked'],
        admin=mongo_user['admin'],
        validatedEmail=mongo_user['validatedEmail'],
        token=token,
        consentTexts=mongo_user['consentTexts'],
    )
    pg_session.add(pg_user)
    pg_session.commit()
    user_ids = models.IDs('user', str(mongo_user['_id']), pg_user.id)
    pg_session.add(user_ids)
    pg_session.commit()
    mongo_forms = mongo.get_user_forms(mongo_db, HOSTNAME, str(mongo_user['_id']))
    for mongo_form in mongo_forms:
        print(f"Importing Form by {pg_user.username}: {mongo_form['slug']}")

        conditions = {  "totalAnswers": mongo_form['expiryConditions']['totalEntries'],
                        "expireDate": mongo_form['expiryConditions']['expireDate'],
                        "fields": mongo_form['expiryConditions']['fields']
                     }
        pg_form = models.Form(
            created=mongo_form['created'],
            author_id=pg_user.id,
            slug=mongo_form['slug'],
            editors=mongo_form['editors'],
            enabled=mongo_form['enabled'],
            expired=mongo_form['expired'],
            sendConfirmation=mongo_form['sendConfirmation'],
            expiryConditions=conditions,
            structure=mongo_form['structure'],
            fieldIndex=mongo_form['fieldIndex'],
            sharedAnswers=mongo_form['sharedEntries'],
            restrictedAccess=mongo_form['restrictedAccess'],
            adminPreferences=mongo_form['adminPreferences'],
            introductionText=mongo_form['introductionText'],
            afterSubmitText=mongo_form['afterSubmitText'],
            expiredText=mongo_form['expiredText'],
            consentTexts=mongo_form['consentTexts'],
        )
        pg_session.add(pg_form)
        pg_session.commit()
        form_ids = models.IDs('form', str(mongo_form['_id']), pg_form.id)
        pg_session.add(form_ids)
        pg_session.commit()
        mongo_responses = mongo.get_form_responses( mongo_db, HOSTNAME,
                                                    str(mongo_form['_id']))
        print(f"Importing form Entries: {mongo_responses.count()}")
        # Name change! We now call 'FormReponse' 'Answer'
        for mongo_form_response in mongo_responses:
            if mongo_form_response['data'] == {}:
                print("Omitting empty response")
                continue
            pg_answer = models.Answer(
                created=mongo_form_response['created'],
                author_id=pg_user.id,
                form_id=pg_form.id,
                marked=mongo_form_response['marked'],
                data=mongo_form_response['data'],
            )
            pg_session.add(pg_answer)
        pg_session.commit()

""" Update form.editor IDs
    Change name newEntry to newAnswer
"""
pg_forms = pg_session.query(models.Form)
for pg_form in pg_forms:
    editors = {}
    for editor_id, value in pg_form.editors.items():
        user_ids = pg_session.query(models.IDs) \
                             .filter_by(model='user', mongo_id=editor_id) \
                             .first()
        preferences = {
            "notification": {
                "newAnswer": value['notification']['newEntry'],
                "expiredForm": value['notification']['expiredForm']
            }
        }
        editors[user_ids.pg_id] = preferences
    pg_form.editors = editors
pg_session.commit()

""" Populate form_logs """
mongo_forms = mongo.get_forms(mongo_db, HOSTNAME)
for mongo_form in mongo_forms:
    print(f"Importing logs for form: {mongo_form['slug']}")
    form_ids = pg_session.query(models.IDs) \
                    .filter_by(model='form', mongo_id=str(mongo_form['_id'])) \
                    .first()
    for log in mongo_form['log']:
        mongo_user = mongo.get_user(mongo_db, HOSTNAME, username=log[1])
        if not mongo_user:
            print(f"Log user not found: {log[2]}")
            continue
        user_ids = pg_session.query(models.IDs) \
                    .filter_by(model='user', mongo_id=str(mongo_user['_id'])) \
                    .first()
        pg_log = models.FormLog(
                created = log[0],
                form_id = form_ids.pg_id,
                user_id = user_ids.pg_id,
                message = log[2]
        )
        pg_session.add(pg_log)
pg_session.commit()

# Drop temporary tables used for migration
models.IDs.__table__.drop(engine)

print("Ok")
