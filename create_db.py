"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from dotenv import load_dotenv
from mongo import mongo
from postgres import postgres
import sys, fileinput

from pprint import pprint

if os.path.exists('sqlalchemy.ini'):
    print("Please delete 'sqlalchemy.ini' first")
    exit()

load_dotenv('.env')
try:
    MONGO_DB = mongo.mongo_db_connect()
except Exception as error:
    print(str(error))
    exit()

def get_site_to_migrate():
    sites = mongo.get_site_names(MONGO_DB)
    for site_name in sites:
        print(site_name)
    name = input("\nEnter name of site you want to migrate: ")
    if not (name and name in sites):
        print("\nNot a valid site name")
        exit()
    # TODO: Ask if this db_name is ok.
    return name

site_to_migrate = get_site_to_migrate()
db_name = f"lf_{site_to_migrate.replace('.', '_')}"

# TODO sanitize
db_user = input("Enter name of the new database user: ")
db_pass = input("Enter password of the new database user: ")

def get_SQLALCHEMY_DATABASE_URI():
    host = os.environ['POSTGRES_HOST']
    dbase = db_name
    port = 5432
    uri = f'postgresql+psycopg2://{db_user}:{db_pass}@{host}:{port}/{dbase}'
    return uri

# Modify alembic.ini
filename = './alembic/alembic.ini'
for line in fileinput.input([filename], inplace=True):
    if line.strip().startswith('sqlalchemy.url ='):
        line = f"sqlalchemy.url = {get_SQLALCHEMY_DATABASE_URI()}\n"
    sys.stdout.write(line)


# Write sqlalchemy.ini varialbes
with open('sqlalchemy.ini', 'w') as f:
    f.write(f'SQLALCHEMY_DATABASE_URI="{get_SQLALCHEMY_DATABASE_URI()}"')
    f.write('\nSQLALCHEMY_TRACK_MODIFICATIONS=False')
    f.write(f'\nSITE_HOST_NAME={site_to_migrate}\n')

POSTGRES = postgres.create_database(db_user=db_user,
                                    db_pass=db_pass,
                                    db_name=db_name)

print("\nNote: Created temporary 'sqlalchemy.ini' file")
