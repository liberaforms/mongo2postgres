"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from pprint import pprint
from pymongo import MongoClient
from bson.objectid import ObjectId


def mongo_db_connect():
    print(os.getenv("MONGO_DB"))
    mongo_client = MongoClient(str(os.getenv("MONGO_HOST")), 27017)
    return mongo_client[os.getenv("MONGO_DB")]

def get_site_names(mongo_db):
    collection = mongo_db.sites
    site_names = []
    for site in collection.find():
        site_names.append(site['hostname'])
    return site_names

def get_installation(mongo_db):
    collection = mongo_db.installation
    return collection.find_one()

def get_site(mongo_db, hostname):
    collection = mongo_db.sites
    return collection.find_one({'hostname': hostname})

def get_invites(mongo_db, hostname):
    collection = mongo_db.invites
    return collection.find({'hostname': hostname})

def get_user(mongo_db, hostname, user_id=None, username=None):
    if not user_id and not username:
        print("mongo.get_user(): user_id or username required")
        exit()
    collection = mongo_db.users
    if user_id:
        return collection.find_one({'hostname': hostname,
                                    '_id': ObjectId(user_id)})
    if username:
        return collection.find_one({'hostname': hostname,
                                    'username': username})

def get_users(mongo_db, hostname):
    collection = mongo_db.users
    return collection.find({'hostname': hostname})

def get_forms(mongo_db, hostname):
    collection = mongo_db.forms
    return collection.find({'hostname': hostname})

def get_user_forms(mongo_db, hostname, user_id):
    collection = mongo_db.forms
    return collection.find({'hostname': hostname, 'author': user_id})

def get_form_responses(mongo_db, hostname, form_id):
    collection = mongo_db.responses
    return collection.find({'hostname': hostname, 'form_id': form_id})
