# LiberaForms mongo2postgres

We have decided to stop using mongoDB in favour of PosgtreSQL. These scripts perform that migration.

> Note: LiberaForms.v1 could host many different domains in the same database. LiberaForms.v2 does not do this. We now use a seperate database for each domain. See liberaforms/docs for more information

Before migrating you must ensure your installation is running the last release version 1.8.13

You can upgrade like this
```
git fetch origin tag v1.8.13 --no-tags
git checkout v1.8.13
```
Now read the docs/UPGRADE

# Install

Check: You are running version 1.8.13 [✔]

## PostgreSQL

Before migrating you need to install PostgreSQL on your server.

`mongo2postgres` needs to know a postgres superuser (root user) name and password
so it can create the LiberaForms database(s).

```
su postgres
psql
```

Now alter the password
```
ALTER USER postgres PASSWORD 'xxxxxxxxx';
```

## mongo2postgres

Log in to your LiberaForms server and install mongo2postgres.

```
cd /tmp
wget https://gitlab.com/liberaforms/mongo2postgres/-/archive/master/mongo2postgres-master.tar.gz
tar zxvf mongo2postgres-master.tar.gz
cd mongo2postgres-master
python3 -m venv ./venv
source ./venv/bin/activate
pip install pip --upgrade
pip install -r ./requirements.txt
```

# Configure

```
cp dotenv.example .env
```

Edit `.env` and add your parameters.


# Migrate

We will not modify the mongo database. However, it is good practice to make a backup.

```
/usr/bin/mongodump --db=LiberaForms --out="/var/backups/"
```

## Create the postgres database

You will be asked for the domain data you wish to migrate.

```
python create_db.py
```

Create the tables

```
alembic -c ./alembic/alembic.ini upgrade head
```

## Migrate the data

This script will print the process to the terminal and finish with "Ok". If any errors are reported, your database has not been migrated correctly.

```
python migrate.py
```

## Multiple domains

If you have more than one domain hosted in your mongodb database, you will need to repeat this migratation process once for each domain name.

This will create another database and migrate the domain data.
```
rm sqlalchemy.ini
python create_db.py
alembic -c ./alembic/alembic.ini upgrade head
python migrate.py
```

Please read https://gitlab.com/liberaforms/cluster to host multiple sites.


# Useful commands

## MongoDB

```
show dbs # show databases
use <database_name> # switch to a database
db.runCommand({ dropDatabase : 1 }) # delete the current database
```

Restore database
```
mongorestore -d <database_name> path/to/dump/files
```

## postgreSQL

As the `postgres` user on your system
```
pg_dump <database_name> > /tmp/<database_name>.pgdump
```
